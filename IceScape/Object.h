#pragma once
#define OBJECT_H

#include <iostream>
#include <Box2D\Box2D.h>
#include <SFML\Graphics.hpp>
#include "Collider.h"

//Type defination that can be used for type checking, NOTE! ADJUST THE ENUM UPON ADDING NEW OBJECT TYPES!
enum ObjectType { E_Object, E_Behaviour, E_Player, E_Ice, E_Particle };

/// ----- | Box2D | ----- \\\
///When creating shapes in Box2D, it creates the shape from the center of the object.

/// ----- | SFML  | ----- \\\
///When creating shapes in SFML, it creates the shape from the top left of the object.
///To match the shape of Box2D, it should either be positioned with the center offset in mind.
///Or The origin must be changed.

//Base class for all objects.
class Object
{
public:

	//Object type.
	ObjectType type;

public:

	//Invokes upon collision entering.
	virtual void PreCollision(b2Contact* contact, const b2Manifold* oldManifold);
	//Invokes upon collision entering.
	virtual void CollisionEnter(b2Body* body);
	//Invokes upon collision exiting.
	virtual void CollisionExit(b2Body* body);
	//Invokes after collision.
	virtual void PostCollision(b2Contact* contact, const b2ContactImpulse* impulse);
	//Invokes when it's ready for destruction.
	virtual void OnDestroy();

	Object();
	~Object();
};

