#include "Object.h"

void Object::PreCollision(b2Contact * contact, const b2Manifold * oldManifold) {}
void Object::CollisionEnter(b2Body * body) {}
void Object::CollisionExit(b2Body * body) {}
void Object::PostCollision(b2Contact * contact, const b2ContactImpulse * impulse) {}
void Object::OnDestroy(){}

Object::Object(){}
Object::~Object(){}