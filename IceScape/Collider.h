#pragma once
#include <Box2D\Box2D.h>

///Forward declaration.
class Object;

class Collider
{
public:
	Object* object;

public:

	//Invokes before collision entering.
	virtual void PreCollision(b2Contact* contact, const b2Manifold* oldManifold);
	//Invokes upon collision entering.
	virtual void CollisionEnter(b2Body* body);
	//Invokes upon collision exiting.
	virtual void CollisionExit(b2Body* body);
	//Invokes after collision.
	virtual void PostCollision(b2Contact* contact, const b2ContactImpulse* impulse);

	Collider(Object* object);
	~Collider();
};

