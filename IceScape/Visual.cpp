#include "Visual.h"
Visual::Visual(b2Vec2 position, float rot, b2Vec2 scale, bool rigid, b2BodyType type, FixtureDefSettings fds) : Behaviour(position, rot, scale, rigid, type, fds)
{
}

void Visual::Start()
{
	if(t_Background.loadFromFile("IceScape_Background1.png"))
	{
		t_Background.setRepeated(true);
		s_Background.setTextureRect(sf::IntRect(-20000, 20000, 40000, 40000));
		s_Background.setTexture(t_Background);
		s_Background.setOrigin(s_Background.getTextureRect().width * 0.5f, s_Background.getTextureRect().height * 0.5f);
		s_Background.setScale(scale.x, scale.y);
		s_Background.setColor(sf::Color(255, 255, 255, 150));
	}
}

void Visual::Update()
{
	Screen::instance->window.draw(s_Background);
}
