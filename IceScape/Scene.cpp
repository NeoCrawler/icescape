#include "Scene.h"
#include "Behaviour.h"

Scene* Scene::instance = 0;

Scene::Scene()
{
	instance = this;
	behaviours = std::vector<Behaviour*>(0);
}

void Scene::AddBehaviour(Behaviour* behaviour)
{
	behaviours.push_back(behaviour);
}

void Scene::RemoveBehaviour(Behaviour* behaviour)
{
	for (size_t i = 0; i < behaviours.capacity(); i++)
	{
		if (behaviours[i] == behaviour)
		{
			behaviours.erase(behaviours.begin() + i);
			delete behaviours[i];
		}	
	}
}

void Scene::AddForRemoval(Object* object)
{
	scheduledForRemoval.push_back(object);
}

void Scene::Reset()
{
	for (Behaviour* behaviour : behaviours)
	{
		behaviour->Reset();
	}
}

void Scene::Remove()
{
	if (!Physics::instance->world.IsLocked())
	{
		for (Object* object : scheduledForRemoval)
		{
			object->OnDestroy();
			
		}

		scheduledForRemoval.clear();
	}	
}

Scene::~Scene()
{
}
