#pragma once
#include "Behaviour.h"
#include "Particle.h"

//The player class deals with all handling that the player is capable of doing.
class Player : public Behaviour
{

private:
	
	//Cooldown spraying.
	int cd_Spray = 30;
	//If true, the player is able to jump.
	bool isGrounded = true;
	//The initial position of when the mouse is clicked.
	b2Vec2 * mouseStart;

	///The visualisation of the water pray system.
	sf::CircleShape hoseStart;
	sf::CircleShape hoseEnd;
	sf::CircleShape hoseSystem;
	sf::VertexArray hoseDirection;

	///Contains the water particles the player capable of spraying.
	std::vector<Particle* > particles;

	///The visualisation of the player.
	sf::Texture t_Player;
	sf::Sprite s_Player;

public:
	void Start();
	void Update();
	void CollisionEnter(b2Body* body);
	void Reset();

	Player(b2Vec2 position, float rot, b2Vec2 scale, bool rigid, b2BodyType type, FixtureDefSettings fds);

private:
	void Movement();
};