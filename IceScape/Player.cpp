#include "Player.h"

void Player::Start()
{
	this->type = E_Player;

	///Set the cursor settings.
	sf::Cursor::Cross;
	Screen::instance->window.setMouseCursorVisible(false);
	Screen::instance->window.setMouseCursorGrabbed(false);

	///Set the graphics settings for the starting point of the hose system.
	hoseStart.setRadius(5.0f);
	hoseStart.setFillColor(sf::Color(0,255,0,100));
	hoseStart.setOutlineColor(sf::Color::Black);
	hoseStart.setOutlineThickness(1.0f);

	///Set the graphics settings for the end point of the hose system.
	hoseEnd.setRadius(3.0f);
	hoseEnd.setFillColor(sf::Color(0, 255, 0, 100));
	hoseEnd.setOutlineColor(sf::Color::Black);
	hoseEnd.setOutlineThickness(1.0f);

	///Set the graphics settings for the directional indecator of the hose system.
	hoseDirection = sf::VertexArray(sf::Lines, 2);
	hoseDirection[0].color = sf::Color::Green; 
	hoseDirection[1].color = sf::Color::Blue;

	///Set the graphics settings for the radius field of the hose system.
	hoseSystem.setFillColor(sf::Color(152, 251, 152, 50));
	hoseSystem.setOutlineThickness(2);
	hoseSystem.setOutlineColor(sf::Color::Black);

	///Load the texture for the player.
	if (t_Player.loadFromFile("IceScape_Player.png"))
	{
		s_Player.setTexture(t_Player);
		s_Player.setOrigin(s_Player.getTextureRect().width * 0.5f, s_Player.getTextureRect().height * 0.5f);
	}

	///Set the collision shape to the size of the texture.
	colliderShape.SetAsBox((float)s_Player.getTextureRect().width, (float)s_Player.getTextureRect().height);
	
	///Make the origional visual non visable.
	visual.setScale(0, 0);
}

void Player::Update()
{
	Behaviour::Update();

	///Set the center of the view on the player.
	sf::View view = Screen::instance->window.getView();
	view.setCenter(rigidBody->GetPosition().x, rigidBody->GetPosition().y);
	Screen::instance->window.setView(view);

	///Get the mouse position is pixels and convert it to units.
	sf::Vector2i pixelPos = sf::Mouse::getPosition(Screen::instance->window);
	sf::Vector2f temp = Screen::instance->window.mapPixelToCoords(pixelPos);

	///Sweep the player to where ever you want, used for debug purpose.
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right))
	{
		rigidBody->SetLinearVelocity(b2Vec2(0, 1));
		rigidBody->SetTransform(b2Vec2(temp.x, temp.y), 0);
	}

	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
	{
		if (mouseStart == nullptr)
		{
			mouseStart = new b2Vec2(temp.x, temp.y);
		}

		///Distance between the starting position and current mouse position.
		float dist = b2Distance(b2Vec2(hoseStart.getPosition().x, hoseStart.getPosition().y), b2Vec2(temp.x, temp.y));
		b2Vec2 dir = b2Vec2(temp.x, temp.y) - b2Vec2(hoseStart.getPosition().x, hoseStart.getPosition().y);
		dir.Normalize();

		///Check whether the radius of the spray system is within range.
		if (dist < 200)
		{
			///Is the radius higher as the minimum.
			if (dist > 50 && cd_Spray >= 10)
			{
				cd_Spray = 0;
				particles.push_back(new Particle(hoseStart.getPosition(), b2Vec2(dir.x * dist, dir.y * dist)));
				std::cout << particles.capacity() << std::endl;
			}

			///Set the radius of the hose system.
			hoseSystem.setRadius(dist + 10);
			float _hsX = hoseStart.getPosition().x + hoseStart.getRadius() - hoseSystem.getRadius();
			float _hsY = hoseStart.getPosition().y + hoseStart.getRadius() - hoseSystem.getRadius();
			hoseSystem.setPosition(_hsX, _hsY);
		}

		///Set the start / end position of the line.
		hoseDirection[0].position = sf::Vector2f(hoseStart.getPosition().x + hoseStart.getRadius(), hoseStart.getPosition().y + hoseStart.getRadius());
		hoseDirection[1].position = sf::Vector2f(hoseEnd.getPosition().x + hoseEnd.getRadius(), hoseEnd.getPosition().y + hoseEnd.getRadius());
		
		hoseEnd.setPosition(temp.x + hoseEnd.getRadius(), temp.y + hoseEnd.getRadius());

		cd_Spray++;

		Screen::instance->window.draw(hoseDirection);
		Screen::instance->window.draw(hoseEnd);
		Screen::instance->window.draw(hoseSystem);
	}
	else
	{
		hoseStart.setPosition(temp.x - hoseStart.getRadius(), temp.y - hoseStart.getRadius());

		delete mouseStart;
		mouseStart = nullptr;
	}

	///Particle loop
	for (size_t i = 0; i < particles.size(); i++)
	{
		///Redirect the update function onwards to the particles.
		particles[i]->Update();

		///Delete the particle if it is to be deleted.
		if (particles[i]->flaggedForDelete)
		{
			particles[i]->OnDestroy();
			particles.erase(particles.begin() + i);
		}
	}

	///If the player falls below a certain position, reset the game.
	if (position.y >= 2500)
		Scene::instance->Reset();

	Movement();

	s_Player.setPosition(position.x, position.y);

	Screen::instance->window.draw(s_Player);
	Screen::instance->window.draw(hoseStart);
}

void Player::Movement()
{
	///up key is pressed: move our character
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && isGrounded)
	{
		rigidBody->ApplyForceToCenter(b2Vec2(0, -20000.0f), true);
		isGrounded = false;
	}

	///up key is pressed: move our character
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		rigidBody->ApplyForceToCenter(b2Vec2(-200.0f, 0), true);
	}

	///up key is pressed: move our character
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		rigidBody->ApplyForceToCenter(b2Vec2(200.0f, 0), true);
	}

	///Clamp the maximum velocity of the player, this 
	rigidBody->SetLinearVelocity(b2Clamp(rigidBody->GetLinearVelocity(), b2Vec2(-3, -3), b2Vec2(3, 10)));
}

void Player::CollisionEnter(b2Body * body)
{
	isGrounded = true;
}

void Player::Reset()
{
	rigidBody->SetLinearVelocity(b2Vec2(0, 1));
	rigidBody->SetTransform(b2Vec2(-1925, 120), 0);
}

Player::Player(b2Vec2 position, float rot, b2Vec2 scale, bool rigid, b2BodyType type, FixtureDefSettings fds) : Behaviour(position, rot, scale, rigid, type, fds)
{

}
