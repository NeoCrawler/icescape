#include "ContactListener.h"

void ContactListener::BeginContact(b2Contact * contact)
{
	((Collider*)contact->GetFixtureA()->GetBody()->GetUserData())->CollisionEnter(contact->GetFixtureB()->GetBody());
	((Collider*)contact->GetFixtureB()->GetBody()->GetUserData())->CollisionEnter(contact->GetFixtureA()->GetBody());
}

void ContactListener::EndContact(b2Contact * contact)
{
	((Collider*)contact->GetFixtureA()->GetBody()->GetUserData())->CollisionExit(contact->GetFixtureB()->GetBody());
	((Collider*)contact->GetFixtureB()->GetBody()->GetUserData())->CollisionExit(contact->GetFixtureA()->GetBody());
}

void ContactListener::PreSolve(b2Contact * contact, const b2Manifold * oldManifold)
{
	//((Behaviour*)contact->GetFixtureA()->GetBody()->GetUserData())->PreCollision(contact, oldManifold);
	//((Behaviour*)contact->GetFixtureB()->GetBody()->GetUserData())->PreCollision(contact, oldManifold);
}

void ContactListener::PostSolve(b2Contact * contact, const b2ContactImpulse * impulse)
{
	//((Behaviour*)contact->GetFixtureA()->GetBody()->GetUserData())->PostCollision(contact, impulse);
	//((Behaviour*)contact->GetFixtureB()->GetBody()->GetUserData())->PostCollision(contact, impulse);

}

ContactListener::ContactListener() {}
