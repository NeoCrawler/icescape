#pragma once
#include "Behaviour.h"
#include "Player.h"
#include "Ice.h"
#include "Visual.h"

int main()
{
	///To actually be able to call virtual functions,
	///The object needs to be a pointer.
	///Which forces the loop to use a pointer vector.
	
	Screen screen(1920, 1080);

	Physics physics;

	Scene scene;

	///-----|| Core systems in place ||-----\\\

	Visual* background = new Visual(b2Vec2(0, 0), 0, b2Vec2(1, 1), false, b2_dynamicBody, FixtureDefSettings::Default());
	Player* player = new Player(b2Vec2(-1925, 120), 0, b2Vec2(11, 11), true,b2_dynamicBody, FixtureDefSettings(false, 1.0f, 0.01f, 0.25f));

	///Level definition.
	new Behaviour(b2Vec2(-1250,200),0,b2Vec2(1500,100), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(-650, 45), 0, b2Vec2(100, 10), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(-450, 45), 0, b2Vec2(100, 10), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(-50, 200), 0, b2Vec2(500, 100), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(1250, 200), 0, b2Vec2(1500, 100), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(1150, -10), 0, b2Vec2(100, 10), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(850, 25), 0, b2Vec2(100, 10), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(4500, -250), 0, b2Vec2(1500, 100), true, b2_staticBody, FixtureDefSettings::Default());

	new Behaviour(b2Vec2(-1975, 100), 0, b2Vec2(50, 100), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(-1000, 137.5f), 0, b2Vec2(25, 25), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(-875, 115), 0, b2Vec2(100, 30), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(-775, 112.5f), 0, b2Vec2(50, 75), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(-650, 137.5f), 0, b2Vec2(25, 25), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(-550, 112.5f), 0, b2Vec2(50, 25), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(-532.5f, 87.5f), 0, b2Vec2(15, 25), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(1250, 50), 0, b2Vec2(50, 200), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(1725, 137.5f), 0, b2Vec2(25, 25), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(1825, 125), 0, b2Vec2(50, 50), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(1962.5f, 112.5f), 0, b2Vec2(75, 75), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(2162.5f, 112.5f), 0, b2Vec2(75, 100), true, b2_staticBody, FixtureDefSettings::Default());
	new Behaviour(b2Vec2(2262.5f, 112.5f), 0, b2Vec2(75, 100), true, b2_staticBody, FixtureDefSettings::Default());

	new Ice(b2Vec2(150,90),0,b2Vec2(0,0), true, b2_staticBody, FixtureDefSettings::Default());
	new Ice(b2Vec2(500, 60), 0, b2Vec2(0, 0), true, b2_staticBody, FixtureDefSettings::Default());
	new Ice(b2Vec2(1300, 60), 0, b2Vec2(0, 0), true, b2_staticBody, FixtureDefSettings::Default());
	new Ice(b2Vec2(1600, 0), 0, b2Vec2(0, 0), true, b2_staticBody, FixtureDefSettings::Default());
	new Ice(b2Vec2(2000, -200), 0, b2Vec2(0, 0), true, b2_staticBody, FixtureDefSettings::Default());
	new Ice(b2Vec2(2200, -200), 0, b2Vec2(0, 0), true, b2_staticBody, FixtureDefSettings::Default());
	new Ice(b2Vec2(2400, -200), 0, b2Vec2(0, 0), true, b2_staticBody, FixtureDefSettings::Default());
	new Ice(b2Vec2(1800, -400), 0, b2Vec2(0, 0), true, b2_staticBody, FixtureDefSettings::Default());
	new Ice(b2Vec2(2200, -400), 0, b2Vec2(0, 0), true, b2_staticBody, FixtureDefSettings::Default());
	new Ice(b2Vec2(2600, -400), 0, b2Vec2(0, 0), true, b2_staticBody, FixtureDefSettings::Default());
	
	for (Behaviour *behaviour : Scene::instance->behaviours)
	{
		behaviour->Start();
	}	

	///Game loop
	while (Screen::instance->window.isOpen())
	{
		Screen::instance->window.clear();

		///Update all behaviours.
		for (Behaviour *behaviour : Scene::instance->behaviours)
		{
			if(behaviour != nullptr)
				behaviour->Update();
		}

		float32 timeStep = 1.0f / 60;
		int velocityIterations = 8;
		int32 positionIterations = 3;

		///Run the physics simulation.
		for (int32 i = 0; i < 60; ++i)
		{
			Physics::instance->world.Step(timeStep, velocityIterations, positionIterations);
			
			///Physics update for all behaviours.
			for (Behaviour *behaviour : Scene::instance->behaviours)
			{
				behaviour->PhysicsUpdate();
			}
		}

		Screen::instance->window.display();

		Scene::instance->Remove();
	}

	return 0;
}