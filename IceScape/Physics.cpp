#include "Physics.h"

Physics* Physics::instance = 0;

Physics::Physics() : world(b2Vec2(0, 0.1f))
{
	instance = this;
	world.SetContactListener(&cl);
}
