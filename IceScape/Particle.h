#pragma once
#include <Box2D\Box2D.h>
#include <SFML\Graphics.hpp>
#include "Object.h"

class Particle : public Object
{
public: 
	sf::CircleShape particleShape;

	///Create a body defination.
	b2BodyDef bodyDefinition;

	Collider* collider;

	///Create a collider shape.
	b2PolygonShape colliderShape;

	///Create the physics object.
	b2Body* rigidBody;
	b2FixtureDef fixtureDef;
	b2Fixture* fixture;

	///Create the visual.
	sf::RectangleShape visual;

	//Should this particle be deleted.
	bool flaggedForDelete = false;
	//The lifespan of the particle.
	int life = 180;

public:
	Particle(sf::Vector2f pos, b2Vec2 force);

	void CollisionEnter(b2Body* body);
	void Update();
	void OnDestroy() override;
};

