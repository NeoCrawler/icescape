#include "Ice.h"
#include <iostream>
#include "Particle.h"

Ice::Ice(b2Vec2 position, float rot, b2Vec2 scale, bool rigid, b2BodyType type, FixtureDefSettings fds) : Behaviour(position, rot, scale, rigid, type, fds)
{
	this->type = E_Ice;

	Reset();

	/// Create the visual.
	visualShape = sf::VertexArray(sf::TriangleFan, vertexCount);

	/// Assign random blue colors to each vertex point.
	for (size_t i = 0; i < vertexCount; i++)
	{
		int range = 2 - 1 + 1;
		int random = rand() % range + 1;

		if(random == 1)
			visualShape[i].color = sf::Color(134, 214, 216);
		else
			visualShape[i].color = sf::Color(185, 232, 234);
	}
}

void Ice::Start()
{
	
}

void Ice::Update()
{
	/// Radomise vertices when butten is pressed.
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		RandomiseVertices();

	/// Update object when object is changed.
	if (isDirty)
		UpdateObject();

	/// Draw the visual to the screen.
	Screen::instance->window.draw(visualShape);
}

void Ice::CollisionEnter(b2Body * body)
{
	/// If the collided object type is target type.
	if(((Collider*)body->GetUserData())->object->type == E_Particle)
	{
		int index = 0;

		isDirty = true;

		b2Vec2* closest = nullptr;

		/// Loop through all 
		for (rsize_t i = 0; i < vertexCount; i++)
		{
			/// If there is no closest point yet, fill it.
			if (closest == nullptr)
			{
				closest = &collShape[i];
				index = i;
			}

			/// If the distance of the next vertex is closer to the collided body
			/// Then the current vertex distance, set the new vertex as closest.
			float distNew = b2Distance(collShape[i] + position, body->GetPosition());
			float distPre = b2Distance(*closest + position, body->GetPosition());

			if (distNew < distPre)
			{
				closest = &collShape[i];
				index = i;
			}
		}

		/// Calculate the direction of the vertex point to the body.
		b2Vec2 dir = -body->GetLinearVelocity();
		dir.Normalize();

		/// Move the vertex point into the direction of the body.
		*closest = *closest + b2Vec2(dir.x * deformationStrength, dir.y * deformationStrength);
	}
}

void Ice::Reset()
{
	isDirty = true;

	/// Create a custom box collider with a decent amount of vertices.
	/// Possible should use a algorithm, but for now this will do.
	collShape[0].Set(position.x + 40, position.y + 0);
	collShape[1].Set(position.x + 20, position.y + 0);

	collShape[2].Set(position.x + 0, position.y + 0);
	collShape[3].Set(position.x + 0, position.y + 20);

	collShape[4].Set(position.x + 0, position.y + 40);
	collShape[5].Set(position.x + 20, position.y + 40);

	collShape[6].Set(position.x + 40, position.y + 40);
	collShape[7].Set(position.x + 40, position.y + 20);
}

/// Update the collider and graphic when any changes are made to the vertices.
void Ice::UpdateObject()
{
	isDirty = false;
	
	/// Update the collision shape. (Expensive operation)
	rigidBody->DestroyFixture(fixture);
	colliderShape.Set(collShape, vertexCount);
	fixture = rigidBody->CreateFixture(&fixtureDef);

	/// Update all vertex points of the visual corrisponding to the collider.
	for (size_t i = 0; i < vertexCount; i++)
	{
		visualShape[i].position.x = position.x + collShape[i].x;
		visualShape[i].position.y = position.y + collShape[i].y;
	}
}


/// Randomise all vertices on the object.
void Ice::RandomiseVertices()
{
	for (size_t i = 0; i < vertexCount; i++)
	{
		isDirty = true;

		int range = 2 - -2 + 1;
		int random = rand() % range + -2;

		collShape[i].Set(collShape[i].x + random, collShape[i].y);

		random = rand() % range + -2;

		collShape[i].Set(collShape[i].x, collShape[i].y + random);
	}
}



