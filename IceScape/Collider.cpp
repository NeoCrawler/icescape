#include "Collider.h"
#include "Object.h"

void Collider::PreCollision(b2Contact * contact, const b2Manifold * oldManifold)
{

}

void Collider::CollisionEnter(b2Body * body)
{
	object->CollisionEnter(body);
}

void Collider::CollisionExit(b2Body * body)
{
}

void Collider::PostCollision(b2Contact * contact, const b2ContactImpulse * impulse)
{
}

Collider::Collider(Object* object)
{
	this->object = object;
}


Collider::~Collider()
{
}
