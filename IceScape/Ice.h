#pragma once
#include "Behaviour.h"

int const vertexCount = 8;

class Ice : public Behaviour
{
public:

	//The strength with which the object deforms.
	float deformationStrength = 15.0f;

private:

	///Update the ice shape if flagged dirty.
	bool isDirty = true;
	///Array that stores all vertices for the visual.
	sf::VertexArray visualShape;
	///Array that stores all vertices for the collider.
	b2Vec2 collShape[vertexCount];

public:

	void Start();
	void Update();
	void CollisionEnter(b2Body* body);
	void Reset() override;

	void UpdateObject();
	void RandomiseVertices();

	Ice(b2Vec2 position, float rot, b2Vec2 scale, bool rigid, b2BodyType type, FixtureDefSettings fds);
};