#pragma once
#include <iostream>
#include <vector>
#include <iterator>
#include "Object.h"
#include "Physics.h"

///Forward declaration.
class Behaviour;

class Scene
{
public:
	
	//Singleton.
	static Scene* instance;
	//List of all current behaviours in the world.
	std::vector<Behaviour*> behaviours;
	//List of all objects ready for removal.
	std::vector<Object*> scheduledForRemoval;

public:
	void AddBehaviour(Behaviour* behaviour);
	void RemoveBehaviour(Behaviour* behaviour);
	void AddForRemoval(Object* object);
	void Remove();
	//Globally calls reset on all behaviours.
	void Reset();
	Scene();
	~Scene();
};

