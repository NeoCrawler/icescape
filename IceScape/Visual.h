#pragma once
#include "Behaviour.h"

class Visual : public Behaviour
{
	sf::Texture t_Background;
	sf::Sprite s_Background;

public:
	Visual(b2Vec2 position, float rot, b2Vec2 scale, bool rigid, b2BodyType type, FixtureDefSettings fds);

	void Start();
	void Update();
};

