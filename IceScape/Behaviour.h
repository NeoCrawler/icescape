#pragma once
#define BEHAVIOUR_H

#include <Box2D/Common/b2Math.h>
#include "Physics.h"
#include "ContactListener.h"
#include "Structs.h"
#include "Screen.h"
#include "Scene.h"
#include "Object.h"

//Base class for all objects that need to run on the main loop.
class Behaviour : public Object
{

public:

	///Basic behaviour properties
	b2Vec2 position;
	float rotation;
	b2Vec2 scale;

	bool rigid;
	Collider* collider;

	///Create a body defination.
	b2BodyDef bodyDefinition;

	///Create a collider shape.
	b2PolygonShape colliderShape;

	///Create the physics object.
	b2Body* rigidBody;
	b2FixtureDef fixtureDef;
	b2Fixture* fixture;
	
	///Create the visual.
	sf::RectangleShape visual;
	
public:
	
	//Invokes before the first frame.
	virtual void Start();
	//Invokes every frame.
	virtual void Update();
	//Invokes every physics substep.
	virtual void PhysicsUpdate();
	//Invokes when global reset is called.
	virtual void Reset();

	//Invokes before collision entering.
	virtual void PreCollision(b2Contact* contact, const b2Manifold* oldManifold);
	//Invokes upon collision entering.
	virtual void CollisionEnter(b2Body* body);
	//Invokes upon collision exiting.
	virtual void CollisionExit(b2Body* body);
	//Invokes after collision.
	virtual void PostCollision(b2Contact* contact, const b2ContactImpulse* impulse);

	//Constructor definaton.
	Behaviour(b2Vec2 position, float rot, b2Vec2 scale, bool rigid, b2BodyType type, FixtureDefSettings fds);
};



