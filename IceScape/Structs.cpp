#include "Structs.h"
FixtureDefSettings::FixtureDefSettings(bool isSensor = false, float density = 1.0f, float friction = 0.3f, float restitution = 0.05f)
{
	this->isSensor = isSensor;
	this->density = density;
	this->friction = friction;
	this->restitution = restitution;
}

FixtureDefSettings FixtureDefSettings::Default()
{
	return FixtureDefSettings();
}