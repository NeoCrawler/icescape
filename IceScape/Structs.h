#pragma once
//Structs for fixture defination settings.
struct FixtureDefSettings
{
	//When enabled it acts as an trigger. The collider wont stop any colliding objects.
	bool isSensor = false;
	//Density, forces influence lower density objects more.
	float density = 1.0f;
	//Friction, the tension between the surface and object, more friction means more force needed to move the object over a surface.
	float friction = 0.3f;
	//The force returned when hitting an surface.
	float restitution = 0.05f;

public:
	
	static FixtureDefSettings Default();

	FixtureDefSettings(bool isSensor, float density, float friction, float restitution);
};

