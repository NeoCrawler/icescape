#pragma once
#include <Box2D\Box2D.h>
#include "ContactListener.h"

class Physics
{
public:
	//Singleton.
	static Physics* instance;
	//The actual physics world.
	b2World world;
	//Collision listener.
	ContactListener cl;

public:

	Physics();
};
