#pragma once
#include <SFML\Graphics.hpp>
class Screen
{
public:

	//Singleton.
	static Screen* instance;
	//Window that servers for 2D rendering.
	sf::RenderWindow window;

	Screen(int width, int height);
	~Screen();
};

