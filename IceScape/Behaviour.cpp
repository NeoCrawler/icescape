#include "Behaviour.h"

///Constructor
Behaviour::Behaviour(b2Vec2 position, float rot, b2Vec2 scale, bool rigid, b2BodyType type, FixtureDefSettings fds)
{
	this->rigid = rigid;
	this->type = E_Behaviour;

	///Set basic parameters
	this->position = position;
	rotation = rot;
	this->scale = scale;

	///If this Behaviour is part of the physics, initialize its physics settings. 
	if (rigid)
	{
		///Bodydefination type static by default.
		switch (type)
		{
		case b2_staticBody:
			bodyDefinition.type = b2_staticBody;
			break;
		case b2_kinematicBody:
			bodyDefinition.type = b2_kinematicBody;
			break;
		case b2_dynamicBody:
			bodyDefinition.type = b2_dynamicBody;
			break;

		default:
			break;
		}

		///Set the data that receives the collision callback.
		collider = new Collider(this);
		bodyDefinition.userData = collider;

		///Bodydefination settings.
		bodyDefinition.position.Set(position.x, position.y);

		///Add rigid body to the simulation world.
		rigidBody = Physics::instance->world.CreateBody(&bodyDefinition);

		///Collider settings.
		colliderShape.SetAsBox(scale.x * 0.5f, scale.y * 0.5f);

		///Create fixture
		///A fixture binds a shape to a body and adds material properties such as density, friction, and restitution.
		///A fixture puts a shape into the collision system(broad - phase) so that it can collide with other shapes.
		fixtureDef.shape = &colliderShape;
		fixtureDef.isSensor = fds.isSensor;
		fixtureDef.density = fds.density;
		fixtureDef.friction = fds.friction;
		fixtureDef.restitution = fds.restitution;
		fixture = rigidBody->CreateFixture(&fixtureDef);
	}

	///Default Visual representation.
	visual.setFillColor(sf::Color(153,204,255,200));
	visual.setOutlineColor(sf::Color::Black);
	visual.setOutlineThickness(1);

	///Add this Behaviour to the scene.
	Scene::instance->AddBehaviour(this);
}


void Behaviour::Start() {}

void Behaviour::Update()
{
	///If this behaviour is rigid, set the position to the physics position.
	if (rigid)
	{
		position.x = rigidBody->GetPosition().x;
		position.y = rigidBody->GetPosition().y;
	}

	///Draw default visual on the correct position on the screen.
	visual.setPosition(sf::Vector2f(position.x - scale.x * 0.5f, position.y - scale.y * 0.5f));
	visual.setSize(sf::Vector2f(scale.x, scale.y));
	Screen::instance->window.draw(visual);
}

void Behaviour::PhysicsUpdate() {}
void Behaviour::Reset() {};

void Behaviour::PreCollision(b2Contact* contact, const b2Manifold* oldManifold) {}
void Behaviour::CollisionEnter(b2Body* body) {}
void Behaviour::CollisionExit(b2Body* body) {}
void Behaviour::PostCollision(b2Contact* contact, const b2ContactImpulse* impulse) {}


