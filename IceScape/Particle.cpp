#include "Particle.h"
#include "Ice.h"
Particle::Particle(sf::Vector2f pos, b2Vec2 force)
{
	this->type = E_Particle;

	collider = new Collider(this);

	bodyDefinition.type = b2_dynamicBody;
	bodyDefinition.userData = collider;
	bodyDefinition.position.Set(pos.x, pos.y);
	rigidBody = Physics::instance->world.CreateBody(&bodyDefinition);
	rigidBody->SetUserData(collider);
	colliderShape.SetAsBox(2.5f, 2.5f);

	fixtureDef.shape = &colliderShape;
	fixtureDef.density = 10.0f;
	fixtureDef.friction = 0.3f;
	fixture = rigidBody->CreateFixture(&fixtureDef);

	particleShape.setOutlineThickness(1);
	particleShape.setOutlineColor(sf::Color::Black);
	particleShape.setFillColor(sf::Color(50, 248, 255, 50));
	particleShape.setRadius(5);

	rigidBody->ApplyForceToCenter(b2Vec2(force.x * 1000, force.y * 1000), true);
}

void Particle::CollisionEnter(b2Body* body)
{
	///We don't want to delete it when colliding with other particles.
	if (((Collider*)body->GetUserData())->object->type != E_Particle)
		flaggedForDelete = true;
}

void Particle::Update()
{
	if ((life = life - 1) <= 0)
		flaggedForDelete = true;

	std::cout << life << std::endl;
	particleShape.setPosition(rigidBody->GetPosition().x, rigidBody->GetPosition().y);
	Screen::instance->window.draw(particleShape);
}

void Particle::OnDestroy()
{
	Physics::instance->world.DestroyBody(rigidBody);
	free(collider);
	free(this);
}
